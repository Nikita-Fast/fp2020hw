### An implementaion of Java-OOP mini-language

This is a homework for functional programming course.

License: LGPL

Author: Bozhnyuk Alexander, bozhnyuks@mail.ru

Ключевые слова не могут являться именами классов. 

Features done:

- 1 Отдельная функция для теста
- 2 build/log не отображается теперь 
- 3 Исправил создание массивов, new type[] не должен парситься, только new type[expr] или new type[] {expr, ... , expr} 
- 4 Переименовал элементы в Ast
- 5 Исправил функции в demoFirst на "из коробки"
- 6 По мелочи: парсинг @Override
- 7 Подправил тип у методов, классов, конструкторов, объявления переменных

Features in progress:

- 1
- 2
- 3 


